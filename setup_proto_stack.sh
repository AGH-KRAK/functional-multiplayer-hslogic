#!/bin/bash
# Only use if Cabal doesn't work or doesn't regenerate files after schema update
STACK_DIR=`stack --stack-yaml stack.yaml path --local-install-root`
PROTO_FILES=`ls ../../schema/`
if [[ $OS == 'Windows_NT' ]]; then
	STACK_DIR=/`stack --stack-yaml stack.yaml path --local-install-root | tr '\' '/' | sed 's/://'`
	../protoc --plugin=protoc-gen-haskell="${STACK_DIR}/bin/proto-lens-protoc.exe" --haskell_out=src --proto_path=../../schema/ ${PROTO_FILES}
else
	protoc --plugin=protoc-gen-haskell="${STACK_DIR}/bin/proto-lens-protoc" --haskell_out=src --proto_path=../../schema/ ${PROTO_FILES}
fi