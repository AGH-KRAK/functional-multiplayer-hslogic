module DefaultConfig(defaultSettings) where

import FM.API(Settings(..))


defaultSettings :: Settings
defaultSettings = Settings
    { stepLength   = 0.05
    , serverAddr   = (127,0,0,1)
    , commandPort  = 21701
    , gamePort     = 21702
    , configPort   = 21703
    , maxClients   = 64
    , maxGsLength  = 1024
    , maxHandshakeLength = 1024
    }
