module Main(main) where

import Control.Concurrent(forkIO, threadDelay)
import Control.Concurrent.STM(atomically)
import Control.Concurrent.STM.TChan(TChan, newTChan)
import qualified Control.Monad.State as State
import Data.List((\\))
import Data.List.Utils(uniq)
import GHC.IO.Handle(Handle, hSetBuffering, BufferMode(NoBuffering))
import qualified System.Process as P
import System.Info(os)

import ClientCommunication(ClientData)
import qualified ClientCommunication as Client
import FmConfig(settings, initialGameState, step, parseCommand, connectClient, disconnectClient)
import FM.API(Settings(stepLength))
import qualified ProtoWrapper as Protobuf
import Proto.Game(Game)
import Proto.Command(Command)

data ServerData = ServerData {
      game             :: Game
    , connectedClients :: [ClientData]
    , tickId           :: Integer
}
initialServerData :: IO ServerData
initialServerData = initialGameState >>= \gs -> return (ServerData gs [] 0)


main :: IO ()
main = do
    -- initialize the pseudoclient instance
        if System.Info.os == "mingw32" then
            P.withCreateProcess (P.proc "./pseudo_client.bat" [])
            { P.std_out = pipe, P.std_in  = pipe }
        else
            P.withCreateProcess (P.proc "./pseudo_client" [])
            { P.std_out = pipe, P.std_in  = pipe }

        $ \(Just psClientIn) (Just psClientOut) _ _ -> do

        -- initialize communications channels and fork accept threads
        clientChan <- atomically newTChan
        commandChan <- atomically newTChan
        forkIO $ Client.acceptClients clientChan
        forkIO $ Client.acceptCommands psClientOut commandChan

        -- load up the initial data based on the FmConfig
        sd <- initialServerData

        -- ensure buffering doesn't clog up pipes
        hSetBuffering psClientOut NoBuffering

        -- run the game proper
        gameLoop clientChan commandChan psClientIn sd
  where
    pipe = P.CreatePipe


gameLoop ::
       TChan ClientData -- send gamestate on the clientSockets
    -> TChan Command    -- receive commands buffered over the last tick
    -> Handle           -- change the locally rendered gamestate via psClientIn
    -> ServerData       -- the last known serverstate
    -> IO ()
gameLoop clientChan commandChan psClientIn serverData = do
    -- update connected users
    newClients <- atomically $ Client.readEntireTChan clientChan
    let clients = uniq $ connectedClients serverData ++ newClients

    -- fetch commands and update the gamestate
    commands <- atomically $ Client.readEntireTChan commandChan
    let connectState = sequence $ map connectClient newClients
    let commandState = sequence $ map parseCommand commands
    let game' = State.execState (commandState >> step dt >> connectState) $ game serverData

    -- send the gamestate to all connected clients
    let gameWire = Protobuf.serializeGame tick game'
    livingClients <- Client.trySend clients gameWire

    -- update the gamestate if we lost any clients
    let disconnectState = sequence $ map disconnectClient $ clients \\ livingClients
    let game'' = State.execState (disconnectState) game'

    -- wait for the next tick
    let microsec = fromIntegral $ round $ dt * 1000000
    threadDelay microsec

    -- loop
    loopAgainWith $ ServerData game'' livingClients newTick
  where
    dt = stepLength settings
    tick = fromIntegral $ tickId serverData
    newTick = (tickId serverData) + 1
    loopAgainWith = gameLoop clientChan commandChan psClientIn
