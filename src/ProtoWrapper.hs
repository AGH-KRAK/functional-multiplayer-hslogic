{-# LANGUAGE OverloadedStrings #-}

module ProtoWrapper
    (
      -- * Serialization
      serializeGame
    , serializeOnlyGame
    , serializeResponse

      -- * Deserialization
    , deserializeCommand
    , deserializeGame
    , deserializeSync
    , deserializeRequest
    , deserializeResponse

      -- * Data generation
    , createGameSync

      -- * Data interpretation
    , commandData

      -- * Debugging
    , dumpObject
    ) where
import Data.ProtoLens (defMessage, encodeMessage, decodeMessage, showMessage)
import Data.ByteString (ByteString)
import Data.Int (Int64)
import Data.Word(Word32)
import Lens.Micro((^.), (&), (.~))
import Proto.Game(Game)
import Proto.Sync(Sync)
import qualified Proto.Sync_Fields as S
import Proto.Command(Command)
import Proto.Handshake(Request, Response, Accept, Reject)
import qualified Proto.Handshake as H
import qualified Proto.Handshake_Fields as H
import qualified Proto.Command as C
import qualified Proto.Command_Fields as C

-- | Given a tickID and a game, returns the serialized data
serializeGame :: Int64 -> Game -> ByteString
serializeGame tick game = serializeSync $ createGameSync tick game

-- | Like serializeGame, but carries no context about tick
serializeOnlyGame :: Game -> ByteString
serializeOnlyGame = encodeMessage

serializeResponse :: Either Reject Accept -> ByteString
serializeResponse = encodeMessage . createResponse

deserializeResponse :: ByteString -> Either String (Maybe H.Response'Response)
deserializeResponse bs = fmap (^. H.maybe'response) (decodeMessage bs :: Either String Response)

-- | Given a wire message, returns the tickID and command held within
-- Returns a Left error if the given message isn't a parsable command
deserializeCommand :: ByteString -> Either String (Int64, Command)
deserializeCommand = deserializeSyncAs S.command

-- | Given a wire message, returns the tickID and game held within
-- Returns a Left error if the given message isn't a parsable game
deserializeGame :: ByteString -> Either String (Int64, Game)
deserializeGame = deserializeSyncAs S.game

deserializeRequest :: ByteString -> Either String Request
deserializeRequest = decodeMessage

-- | creates a Sync containing just a game, without serializing it
createGameSync :: Int64 -> Game -> Sync
createGameSync tick game = defMessage
    & S.tickId  .~ tick
    & S.game .~ game

createResponse :: Either Reject Accept -> Response
createResponse (Left reject)  = defMessage & H.reject .~ reject
createResponse (Right accept) = defMessage & H.accept .~ accept


commandData :: Command -> Maybe C.Command'CommandData
commandData command = command ^. C.maybe'commandData

dumpObject::Sync -> String
dumpObject message = showMessage message

-- explicitly wrap encode/decode so we don't have to typecase
serializeSync :: Sync -> ByteString
serializeSync message = encodeMessage message

deserializeSync :: ByteString -> Either String Sync
deserializeSync wire_mess = decodeMessage wire_mess

deserializeSyncAs lens wire_mess = case deserializeSync wire_mess of
    Left err -> Left err
    Right sync -> Right (sync ^.S.tickId, sync ^. lens)

-- createExampleSync:: Int64 -> Sync
-- createExampleSync tick = defMessage
--   & S.tickId  .~ tick
--   & S.command .~ command
--     where
--       command :: Command
--       command = defMessage
--           & C.playerId .~ 1
--           & C.use .~ useCommand
--       useCommand :: C.Use
--       useCommand = defMessage
--           & C.object .~ 1

