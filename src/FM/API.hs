{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module FM.API where
import Data.Word(Word8)
import Network.Socket(PortNumber, Socket, SockAddr)

data Settings = Settings
    { stepLength    :: Double
    , serverAddr    :: (Word8, Word8, Word8, Word8)
    , commandPort   :: PortNumber
    , gamePort      :: PortNumber
    , configPort    :: PortNumber
    , maxClients    :: Int
    , maxGsLength   :: Int
    , maxHandshakeLength :: Int
    } deriving (Show)

data ClientData = ClientData {
      clientSocket :: Socket
    , clientAddress :: SockAddr
    } deriving (Eq, Show)
