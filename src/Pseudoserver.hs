module Main(main) where
import System.IO(stdout, hFlush, hPutStrLn, stderr, isEOF)
import qualified Data.ByteString.Char8 as BSC
import qualified Data.ByteString.Base16 as Hex

import FmConfig(settings, initialGameState)
import qualified FM.API as FM
import Networking(Socket, connectSocket, send, recv, tcp, close)

import Control.Concurrent.STM(STM, atomically)
import Control.Concurrent.STM.TChan(TChan, writeTChan, tryReadTChan, newTChan)
import Control.Concurrent.STM.TVar(TVar, writeTVar, readTVar, newTVar)
import Control.Monad(forever, unless)
import Control.Lens(view)
import Control.Concurrent(forkIO)

import qualified ProtoWrapper as Protobuf
import Proto.Sync(Sync)
import qualified Proto.Sync_Fields as S
import qualified Proto.Handshake as H

-- | perform initialization and call the main loop
main :: IO ()
main = do
    -- init socket
    gameSocket <- connectSocket tcp serverAddress gamePort
    -- negotiate with the server
    requestHex <- BSC.getLine
    send gameSocket $ (fst . Hex.decode) requestHex
    responseWire <- recv gameSocket bufferSize
    printToClient responseWire
    -- verify we were accepted
    case Protobuf.deserializeResponse responseWire of
        Left err -> error "Could not parse server response"
        Right response -> case response of
            Nothing -> error "Unknown server response"
            Just (H.Response'Reject reject) -> do
                close gameSocket
            Just (H.Response'Accept accept) -> do
                -- and start the threads
                sync <- initialSync
                gameVar <- atomically $ newTVar sync
                gameChan <- atomically newTChan
                forkIO $ fetchGameLoop gameSocket gameChan gameVar
                serveClient gameChan gameVar
  where
    bufferSize = FM.maxHandshakeLength settings
    serverAddress =  FM.serverAddr settings
    gamePort = FM.gamePort settings


serveClient :: TChan Sync -> TVar Sync -> IO ()
serveClient gsChan gsVar = untilEOF $ do
    timeStr <- getLine
    let time = round $ read timeStr
    maybeSync <- atomically $ firstWhere (\s -> view S.tickId s >= time) gsChan
    sync <- case maybeSync of
        Just s -> pure s
        Nothing -> atomically $ readTVar gsVar
    printToClient $ Protobuf.serializeOnlyGame $ view S.game sync
  where
    untilEOF doIO = forever $ isEOF >>= \eof -> unless eof doIO


fetchGameLoop :: Socket -> TChan Sync -> TVar Sync -> IO ()
fetchGameLoop gameSocket gsChan gsVar = do
    forever $ do
        wire <- recv gameSocket bufferSize
        case Protobuf.deserializeSync wire of
            Left err -> printErr err
            Right sync -> atomically $ do
                writeTChan gsChan sync
                writeTVar gsVar sync
  where
    bufferSize = FM.maxGsLength settings


printToClient :: BSC.ByteString -> IO ()
printToClient bs = (BSC.putStrLn . Hex.encode) bs >> hFlush stdout


printErr = hPutStrLn stderr


firstWhere :: (a -> Bool) -> TChan a -> STM (Maybe a)
firstWhere predicate tchan = do
    val <- tryReadTChan tchan
    case val of
        Just x -> if predicate x
                  then return (Just x)
                  else firstWhere predicate tchan
        Nothing -> return Nothing


initialSync :: IO Sync
initialSync = do
    game <- initialGameState
    return $ Protobuf.createGameSync 0 game

