module ClientCommunication
    (
    -- * Data Type
    ClientData,

    -- * accept threads
    acceptClients,
    acceptCommands,

    -- * helpers for client interactions
    readEntireTChan,
    trySend
    ) where

import Control.Concurrent(forkIO)
import Control.Concurrent.STM(STM, atomically)
import Control.Concurrent.STM.TChan(TChan,
    writeTChan, tryReadTChan)
import Control.Exception(SomeException(..), catch)
import Control.Monad(forever)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Base16 as Hex
import GHC.IO.Handle(Handle)
import qualified System.IO as IO

import FM.API(Settings(maxClients, gamePort, maxHandshakeLength),
              ClientData(ClientData, clientSocket))
import FmConfig(settings, respond)
import Networking(makeSocket, tcp, accept, send, recv, close)
import qualified ProtoWrapper as Protobuf
import Proto.Command(Command)


-- | forever, accepts client connections and queues them as they come
acceptClients :: TChan ClientData -> IO ()
acceptClients tchan = do
    gameSocket <- makeSocket tcp port clientCount
    forever $ do
        (sock, addr) <- accept gameSocket
        let cd = ClientData sock addr
        forkIO $ do
            accepted <- negotiateClient cd
            if accepted
            then atomically $ writeTChan tchan cd
            else close sock
  where
    clientCount = Just $ maxClients settings
    port = gamePort settings


negotiateClient :: ClientData -> IO Bool
negotiateClient cd = do
    -- receive and process request from client
    requestBS <- recv sock $ maxHandshakeLength settings
    let request = case Protobuf.deserializeRequest requestBS of
                      Left err -> error err
                      Right r -> r

    -- return a response
    response <- respond cd request
    let responseBS = Protobuf.serializeResponse response
    send sock responseBS

    -- return whether we should continue this interaction
    case response of
        Right accept' -> return True
        Left reject' -> return False
  where
    sock = clientSocket cd


-- | forever, reads from the engine instance stdout and queues commands
acceptCommands ::
       Handle         -- the engine instance stdout pipe
    -> TChan Command  -- the channel to output commands on
    -> IO ()
acceptCommands engine_out tchan = forever $ do
    commandHex <- BS.hGetLine engine_out
    -- the first component of the following tuple is the tickID
    -- we will eventually want to buffer future ticks and send them when needed
    -- but for now, we don't use it.
    let (_, command) = commFromBS $ fst $ Hex.decode commandHex
    atomically $ writeTChan tchan command
  where
    commFromBS bs = case Protobuf.deserializeCommand bs of
        Left err -> error err
        Right c -> c


-- | reads iteratively through the channel until the channel is empty
-- compiles the results into a list in the channel-provided order
readEntireTChan :: TChan a -> STM [a]
readEntireTChan tchan = do
    val <- tryReadTChan tchan
    case val of
        Just x -> do
            xs <- readEntireTChan tchan
            return (x:xs)
        Nothing -> return []


-- | sends a message to all provided clients,
-- or prints errors to stderr.
-- returns only those clients whose connections are active
trySend :: [ClientData] -> BS.ByteString -> IO [ClientData]
trySend [] _ = return []
trySend (client:xs) wire =
    catch ( do
        let sock = clientSocket client
        send sock wire
        clients <- trySend xs wire
        return (client:clients)
    )
    (\(SomeException ex) -> do
        IO.hPutStrLn IO.stderr (show ex)
        trySend xs wire
    )