module Networking
    (
    -- * types
      AddressTuple
    , Socket
    , SockAddr
    , tcp -- alias for Stream
    , udp -- alias for Datagram

    -- * socket creation
    , makeSocket
    , connectSocket

    -- * use sockets
    , accept
    , send
    , recv

    -- * cleanup
    , close
    ) where

import Network.Socket hiding (send, recv)
import qualified Network.Socket.ByteString as BS
import Data.ByteString(ByteString, empty, snoc, unsnoc, singleton)
import Data.Word(Word8)
import Control.Monad(unless)
import Data.Maybe(fromJust)


type AddressTuple = (Word8, Word8, Word8, Word8)
tcp=Stream
udp=Datagram


send :: Socket -> ByteString -> IO Int
send sock bs = BS.send sock bs'
  where bs' = bs `snoc` 0

recv :: Socket -> Int -> IO ByteString
recv sock buf = do
    bs' <- BS.recv sock buf
    if bs' == empty
        then error "Socket closed or no data received"
        else do
            let (bs, _) = fromJust $ unsnoc bs'
            return bs

-- | creates a local socket and binds it.
-- If needed, can listen to tcp sockets as well.
makeSocket ::
    SocketType -- stream or datagram
    -> PortNumber  -- port
    -> Maybe Int -- number of connections to listen for (for tcp)
    -> IO Socket
makeSocket sType port listenCount = do
    let hints = defaultHints {
          addrFlags = [AI_PASSIVE]
        , addrFamily = AF_INET
        , addrSocketType = sType
        }
    addrInfo <- getAddrInfo (Just hints) Nothing (Just $ show port)
    let address = head addrInfo -- any will work, so pick the simplest
    sock <- socket (addrFamily address) sType (addrProtocol address)
    bind sock $ addrAddress address
    unless (listenCount == Nothing || sType /= Stream)
        $ listen sock (fromJust listenCount)
    return sock


-- | connects to the specified remote socket
connectSocket :: SocketType -> AddressTuple -> PortNumber -> IO Socket
connectSocket sType hostAddr port = do
    sock <- socket AF_INET sType defaultProtocol
    let address = SockAddrInet port $ tupleToHostAddress hostAddr
    connect sock address
    return sock
