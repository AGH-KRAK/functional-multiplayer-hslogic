{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
module FmConfig
    (
    -- * Global Settings
      settings

    -- * Game State
    , initialGameState
    , step

    -- * Client connection handling
    , respond
    , connectClient
    , disconnectClient

    -- * Command handling
    , parseCommand
    ) where

import Control.Monad.State(State)
import Data.ProtoLens (defMessage)
import FM.API(Settings(..), ClientData(..))
import DefaultConfig(defaultSettings)
import Proto.Command(Command)
import qualified Proto.Command as C
import Proto.Handshake(Request, Reject, Accept)
import Proto.Game(Game)
import qualified ProtoWrapper as Protobuf



-- ===== SETTINGS =====

settings :: Settings
settings = defaultSettings



-- ===== GAME STATE =====

-- | Load in the game when the server starts up
initialGameState :: IO Game
initialGameState = return defMessage

-- | Change the gamestate based on time passing
step :: Double -> State Game ()
step dt = return ()



-- ===== CLIENT CONNECTION NEGOTIATON =====

-- | Decide whether the client is allowed in, give them data
respond :: ClientData -> Request -> IO (Either Reject Accept)
respond clientData request = return $ Right defMessage

-- | Modify the game when a client connects
connectClient :: ClientData -> State Game ()
connectClient clientData = return ()

-- | Modify the game when a client's connection drops
disconnectClient :: ClientData -> State Game ()
disconnectClient clientData = return ()



-- ===== COMMANDS =====

-- | Interpret the command to show how it will change the gamestate
parseCommand :: Command -> State Game ()
parseCommand command = case Protobuf.commandData command of
    Just (C.Command'Move move) -> return ()
    Nothing -> error "Invalid command type"


