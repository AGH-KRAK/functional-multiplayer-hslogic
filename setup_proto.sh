#!/bin/bash
# Only use if Cabal doesn't work or doesn't regenerate files after schema update

CABAL_BINDIR=`cabal user-config diff | grep symlink-bindir | sed 's/.*: //'`
LENS_PATH=${CABAL_BINDIR}/proto-lens-protoc
SCHEMA="../../schema"

protoc --plugin=protoc-gen-haskell=$LENS_PATH --haskell_out=src --proto_path=$SCHEMA/ $SCHEMA/*.proto
